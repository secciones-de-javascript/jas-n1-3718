"use strict";
console.warn("PRACTICA 2");
/*
Una compañía decide realizar un ajuste de sueldo a sus 
empleados, para lo cual aplica la siguiente política:

a.- Si tiene más de 5 años de servicio en la empresa 
tendrá un aumento del 10% del sueldo base.
b.- Si tiene como mínimo 4 cursos realizados se 
le otorga un bono de mejoramiento de 25 Bs.

Desarrolle un programa que lea los siguientes datos de un empleado: nombre, sueldo base, años de servicio, números de cursos realizados, debe imprimir nombre, sueldo base, años de servicio, número de cursos y nuevo sueldo del empleado. 
*/

var nombre = "Jose L. Rojas D.";
var sueldoBase = Math.random()*(1000-100)+100;
var añosServicio = Math.round(Math.random()*(20-0)+0);
var numeroCursos = Math.round(Math.random()*(15-1)+1);

console.log(`el sueldo base es ${sueldoBase}`)
console.log(`el numero de años de servicio es ${añosServicio}`)
console.log(`el numero de cursos es ${numeroCursos}`)

var aumento = 0;

if (añosServicio > 5){  // !(años<=5)
    aumento = sueldoBase*0.1;
    console.warn("Le toca aumento por años de servicio")
}

if (numeroCursos >= 4){ // !(aumento<3)
    aumento += 25;
    console.warn("Le toca aumento por número de cursos")
}

if (aumento == 0) // (!(aumento>0))
    console.error("NO le toca aumentos");
else{
    var nuevoSueldo = sueldoBase + aumento;
    var porAumento = aumento/sueldoBase*100;
    porAumento = porAumento.toFixed(2);

    console.log(`El sueldo aumentado es ${nuevoSueldo}`)
    console.log(`Le dimos un aumento del ${porAumento}%`)
}