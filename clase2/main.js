"use strict";
console.warn("PRACTICA 1");

console.log("Practica 1 de la clase 2")
const MIN_EDAD = 10;
const MAX_EDAD = 80;

var edad2,edad3;
var edad1 = Math.random()*(MAX_EDAD-MIN_EDAD)+MIN_EDAD;
// valor original
console.log(edad1)

edad2 = Math.round(edad1);
// valor redondeado pero siendo numero
console.log(edad2)

edad3 = edad2.toFixed();
// el valor sin decimales como string
console.log(edad3)

console.log(`la edad es: ${edad3}`)