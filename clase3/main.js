"use strict"

var precio = prompt("¿Cuál es el precio?")
if (precio==null || precio =="")
    alert("No introdujiste el precio")
else{
    precio = parseFloat(precio)
    if (isNaN(precio))
        alert("Introdujiste un valor que no es numero")
    else
        if (precio < 0)
            alert("El precio debe ser un numero positivo")
        else{
            var descuento = 0
            if (confirm("El producto tiene descuento ?")) {
                descuento = precio*0.10
                alert(`El descuento es ${descuento}`)
            }
            var montoPagar = precio - descuento;
            alert(`El monto a pagar es ${montoPagar}`)
        }
}