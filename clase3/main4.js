"use strict"

let nombre;
let precio;
let descuento=0;
let montoPagar;

procesarProductos();

function procesarProductos(){
    
    for (let i=1;i<=3;i++){
        
        leerNombre();
        leerPrecio();
        calcularDescuento();
        
        montoPagar = precio - descuento;
        
        mostrarResultados();
    }
}

function leerNombre(){
    let valido=false;
    do{
        nombre=prompt(`Nombre del articulo ${i}`);
        if (nombre == null || nombre=="")
            alert("No introjiste el nombre");
        else
            valido=true;
    }while (!valido);
}

function leerPrecio(){
    let valido=false;
    do{
        precio = prompt("¿Cuál es el precio?")
        if (precio==null || precio =="")
            alert("No introdujiste el precio")
        else{
            precio = parseFloat(precio)
            if (isNaN(precio))
                alert("Introdujiste un valor que no es numero")
            else
                if (precio < 0)
                    alert("El precio debe ser un numero positivo")
                else
                    valido = true;
        }
    }while(!valido);
}

function calcularDescuento(){
    if (confirm("El producto tiene descuento ?")) 
        descuento = precio*0.10
}

function mostrarResultados(){
    alert(`Para el producto ${nombre.toUpperCase()} \n`+
            `El descuento es ${descuento.toFixed(2)} \n`+
            `El monto a pagar es ${montoPagar.toFixed(2)}`)
}