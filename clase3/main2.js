"use strict"

for (let i=1;i<=3;i++){
    var valido=false;
    do{
        var nombre=prompt(`Nombre del articulo ${i}`);
        if (nombre == null || nombre=="")
            alert("No introjiste el nombre");
        else
            valido=true;
    }while (!valido);

    valido=false;
    do{
        var precio = prompt("¿Cuál es el precio?")
        if (precio==null || precio =="")
            alert("No introdujiste el precio")
        else{
            precio = parseFloat(precio)
            if (isNaN(precio))
                alert("Introdujiste un valor que no es numero")
            else
                if (precio < 0)
                    alert("El precio debe ser un numero positivo")
                else
                    valido = true;
        }
    }while(!valido);

    var descuento = 0
    if (confirm("El producto tiene descuento ?")) 
        descuento = precio*0.10
    
    var montoPagar = precio - descuento;
    alert(`Para el producto ${nombre.toUpperCase()} \n`+
          `El descuento es ${descuento.toFixed(2)} \n`+
          `El monto a pagar es ${montoPagar.toFixed(2)}`)
}
console.log(i)