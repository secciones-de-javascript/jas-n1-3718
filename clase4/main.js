"use strict"

/*
    las funciones que son llamadas desde los eventos
    (tambien llamadas manejadores de eventos)
    acceden a variables globales y rara vez retornan
    valor.

    Las funciones que son llamadas desde los manejadores
    de eventos, reciben parametros y generalmente
    retornan valor
*/
let diasSemana = ["Lunes","Martes","Miercoles","Jueves",
                   "Viernes"]
let arrNombres=[]
let arrEdades=[]
let arrNroHijos=[]
let arrDiasFavoritos=[]

function leerDiaFavorito(){
    let dias="\n";
    for (let i=0;i<diasSemana.length;i++)
        dias+= i +" -> "+diasSemana[i]+ "\n"

    let dia=leerNumero("Escriba un numero asociado a "+
                        "su dia "+
                        "favorito: "+dias)
    
    //alert("Su dia favorito es "+diasSemana[dia])
    return diasSemana[dia];
}

console.log("código de ejecución directa")

function onLoadPage(){
    console.log("Onload de la pagina")
}

function leerNumero(texto){
    let repetir=true;
    do{
        let numero = parseFloat(prompt(texto))
        if (!isNaN(numero))
            return numero;
        else
            alert("Debe escribir un numero")
    }while(repetir)
}

function leerTexto(titulo){
    let texto=prompt(titulo);
    if (texto != null &&
        texto.trim().length == 0){
        alert("Error. Debe escribir el nombre")
        return null;
    }else
        return texto
}

function procesarDatos(){
    
    if (arrNombres.length == 0)
        alert("No ha registrdo a nadie")
    else{
        let acumHijos=0;
        for (let i=0;i<arrNroHijos.length;i++)
            acumHijos+=arrNroHijos[i]

        let promedio = acumHijos / arrNroHijos.length;
        alert(`El promedio de numeros de hijos es ${promedio}`)
    }    
}

function registrarPersona(){
    let nombre=leerTexto("Introduzca el nombre")
    if (nombre != null){
        let apellido=leerTexto("Introduzca el apellido:")

        nombre+=(apellido !=  null)?" "+apellido:""
        nombre=nombre.toUpperCase()

        if (arrNombres.indexOf(nombre)!=-1)
            alert(`Esta persona (${nombre}) ya fue registrada`)
        else{
            let edad=leerNumero("Introduzca la edad:")
            let nroHijos=leerNumero("Introduzca el nro de hijos:")
            let dia = leerDiaFavorito();
        
            arrNombres.push(nombre)
            arrEdades.push(edad)
            arrNroHijos.push(nroHijos)
            arrDiasFavoritos.push(dia)
        
            alert(`Registrado ${nombre} `+
                   ` que tiene ${edad} años y `+
                    ` ${nroHijos} hijos` )
        }
    }
}